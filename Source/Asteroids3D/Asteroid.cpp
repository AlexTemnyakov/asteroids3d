// Fill out your copyright notice in the Description page of Project Settings.


#include "Asteroid.h"

#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "EntityWidgetComponent.h"
#include "AsteroidsSettings.h"
#include "PlayerSpaceship.h"

AAsteroid::AAsteroid()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>("CollisionSphere");
	CollisionSphere->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	WidgetComponent = CreateDefaultSubobject<UEntityWidgetComponent>("WidgetComponent");
	WidgetComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AAsteroid::BeginPlay()
{
	Super::BeginPlay();

	if (const UAsteroidsSettings* AsteroidsSettings = UAsteroidsSettings::Get())
	{
		
		
		const float Scale = GetActorScale3D().X;
		
		Health = Scale * AsteroidsSettings->ScaleToHealthFactor;
		CollisionDamage = Scale * AsteroidsSettings->ScaleToDamageFactor;
		
		Speed = FMath::FRandRange(AsteroidsSettings->MinSpeed, AsteroidsSettings->MaxSpeed);
	}

	{
		FVector DirToCenter = GetActorLocation();
		DirToCenter.Normalize();

		Direction = FMath::VRandCone(DirToCenter, 70.0f);
	}

	{
		OnActorHit.AddDynamic(this, &AAsteroid::OnActorHitEvent);
		OnActorBeginOverlap.AddDynamic(this, &AAsteroid::OnActorBeginOverlapEvent);
	}
}

void AAsteroid::DestroyAsteroid()
{
	// switch (Reason)
	// {
	// 	
	// }

	Destroy();
}

void AAsteroid::OnActorHitEvent(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	//UE_LOG(LogTemp, Warning, TEXT("AAsteroid::OnActorHitEvent"));
	if (bAreHitsDisabled)
		return;
	
	if (OtherActor)
	{
		bool bActuallyHit = false;
		
		if (AAsteroid* OtherActorAsAsteroid = Cast<AAsteroid>(OtherActor))
		{
			OtherActorAsAsteroid->ApplyDamage(CollisionDamage);
			bActuallyHit = true;
		}
		else if (APlayerSpaceship* OtherActorAsPlayer = Cast<APlayerSpaceship>(OtherActor))
		{
			UE_LOG(LogTemp, Warning, TEXT("A hit with the player."));
			OtherActorAsPlayer->ApplyDamage(CollisionDamage);
			bActuallyHit = true;
		}

		if (bActuallyHit)
		{
			Direction = Hit.Normal;
			bAreHitsDisabled = true;
			FTimerHandle TimerHandle;
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AAsteroid::EnableHits, 0.5f, false);
		}
	}
}

void AAsteroid::OnActorBeginOverlapEvent(AActor* OverlappedActor, AActor* OtherActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("AAsteroid::OnActorBeginOverlapEvent"));
	if (OtherActor)
	{
		if (OtherActor->ActorHasTag("MapBorder"))
			DestroyAsteroid();
	}
}

void AAsteroid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	{
		const FVector LocalMove = Direction * Speed * DeltaTime;
		AddActorWorldOffset(LocalMove, false);
	}

	if (Health <= 0)
		DestroyAsteroid();
}

void AAsteroid::ApplyDamage(const float AppliedDamage)
{
	Health -= AppliedDamage;
}

