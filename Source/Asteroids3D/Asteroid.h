// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Asteroids3DEntityInterface.h"
#include "AsteroidsSettings.h"
#include "Asteroid.generated.h"

UCLASS()
class ASTEROIDS3D_API AAsteroid : public AStaticMeshActor, public IAsteroids3DEntityInterface
{
	GENERATED_BODY()
	
public:	
	AAsteroid();


	virtual void Tick(float DeltaTime) override;

	virtual void ApplyDamage(const float AppliedDamage) override;
	
	
protected:
	virtual void BeginPlay() override;


private:
	void DestroyAsteroid();
	
	UFUNCTION()
	void OnActorHitEvent(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnActorBeginOverlapEvent(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
	void EnableHits() { bAreHitsDisabled = false; }
	

	UPROPERTY(EditAnywhere)
	class USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(EditAnywhere)
	class UEntityWidgetComponent* WidgetComponent = nullptr;

	UPROPERTY(EditAnywhere)
	float Speed;

	UPROPERTY(EditAnywhere)
	bool bAreHitsDisabled = false;

	UPROPERTY(EditAnywhere)
	FVector Direction;

	// This action crashes the project :(.
	//UPROPERTY()
	//const UAsteroidsSettings* AsteroidsSettings = UAsteroidsSettings::Get();
};
