// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Asteroids3D : ModuleRules
{
	public Asteroids3D(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "UMG" });
	}
}
