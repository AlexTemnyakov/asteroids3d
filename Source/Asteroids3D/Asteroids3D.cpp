// Copyright Epic Games, Inc. All Rights Reserved.

#include "Asteroids3D.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Asteroids3D, "Asteroids3D");

DEFINE_LOG_CATEGORY(LogFlying)
