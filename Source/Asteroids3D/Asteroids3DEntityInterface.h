// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Asteroids3DEntityInterface.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UAsteroids3DEntityInterface : public UInterface
{
	GENERATED_BODY()
};

class IAsteroids3DEntityInterface
{    
	GENERATED_BODY()

public:
	virtual void ApplyDamage(const float AppliedDamage) {}

	virtual float GetHealth() { return Health; }
	

protected:
	float Health = 100.0f;	
	float CollisionDamage = 50.0f;
};