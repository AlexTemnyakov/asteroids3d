// Copyright Epic Games, Inc. All Rights Reserved.

#include "Asteroids3DGameMode.h"

#include "AsteroidsSpawnerComponent.h"
#include "Asteroid.h"
#include "EnemySpaceshipsSpawnerComponent.h"
#include "GameDeveloperSettings.h"
#include "PlayerSpaceship.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

AAsteroids3DGameMode::AAsteroids3DGameMode()
{
	PrimaryActorTick.bCanEverTick = false;
	
	DefaultPawnClass = APlayerSpaceship::StaticClass();

	AsteroidsSpawnerComponent = CreateDefaultSubobject<UAsteroidsSpawnerComponent>("Asteroids Spawner Component");

	EnemySpaceshipsSpawnerComponent = CreateDefaultSubobject<UEnemySpaceshipsSpawnerComponent>("Enemies Spawner Component");
}

void AAsteroids3DGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);	
}

void AAsteroids3DGameMode::BeginPlay()
{
	Super::BeginPlay();

	const UGameDeveloperSettings* GameDeveloperSettings = UGameDeveloperSettings::Get();
	
	UUserWidget* CreatedWidget = CreateWidget(GetWorld(), GameDeveloperSettings->HUDClass.Get());
	if (CreatedWidget)
		CreatedWidget->AddToViewport();

	UGameplayStatics::GetPlayerPawn(this, 0)->OnDestroyed.AddDynamic(this, &AAsteroids3DGameMode::OnPlayerDied);

	GetWorld()->GetFirstPlayerController()->InputComponent->BindAction("Exit", IE_Pressed, this, &AAsteroids3DGameMode::OnEscapeButtonPressed);
}

void AAsteroids3DGameMode::OnEscapeButtonPressed()
{
	FGenericPlatformMisc::RequestExit(false);
}

void AAsteroids3DGameMode::OnPlayerDied(AActor* DestroyedActor)
{
	FGenericPlatformMisc::RequestExit(false);
}
