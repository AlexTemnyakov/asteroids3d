// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Asteroids3DGameMode.generated.h"

UCLASS(MinimalAPI)
class AAsteroids3DGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAsteroids3DGameMode();

	
	virtual void Tick(float DeltaSeconds) override;


protected:
	virtual void BeginPlay() override;


private:
	UFUNCTION()
	void OnEscapeButtonPressed();

	UFUNCTION()
	void OnPlayerDied(AActor* DestroyedActor);

	
	UPROPERTY(EditAnywhere)
	class UAsteroidsSpawnerComponent* AsteroidsSpawnerComponent = nullptr;

	UPROPERTY(EditAnywhere)
	class UEnemySpaceshipsSpawnerComponent* EnemySpaceshipsSpawnerComponent = nullptr;
};



