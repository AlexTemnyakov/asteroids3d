// Fill out your copyright notice in the Description page of Project Settings.


#include "Asteroids3DHUD.h"

#include "PlayerSpaceship.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

void UAsteroids3DHUD::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (!Player)
		Player = Cast<APlayerSpaceship>(UGameplayStatics::GetPlayerPawn(this, 0));
	else
		Update(Player->GetHealth(), static_cast<int>(GetWorld()->GetTimeSeconds()));
}

void UAsteroids3DHUD::Update(const int InHealth, const int InGameTime)
{
	HealthTextBlock->SetText(FText::FromString(FString::Printf(TEXT("%d"), InHealth)));

	int Seconds = InGameTime % 60;
	int Minutes = InGameTime / 60;
	MinutesTextBlock->SetText(FText::FromString(FString::Printf(TEXT("%02d"), Minutes)));
	SecondsTextBlock->SetText(FText::FromString(FString::Printf(TEXT("%02d"), Seconds)));
}
