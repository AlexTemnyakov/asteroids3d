// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Asteroids3DHUD.generated.h"

/**
 * 
 */
UCLASS()
class ASTEROIDS3D_API UAsteroids3DHUD : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	

private:
	void Update(const int InHealth, const int InGameTime);

	
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* HealthTextBlock = nullptr;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* MinutesTextBlock = nullptr;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* SecondsTextBlock = nullptr;

	UPROPERTY()
	class APlayerSpaceship* Player = nullptr;
};
