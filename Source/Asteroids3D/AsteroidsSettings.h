// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "AsteroidsSettings.generated.h"

/**
 * 
 */
UCLASS(Config=Game, defaultconfig) 
class ASTEROIDS3D_API UAsteroidsSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	static const UAsteroidsSettings* Get();
	
	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Scale")
	float MinScale = 1.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Scale")
	float MaxScale = 1.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Damage")
	float ScaleToDamageFactor = 30.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Health")
	float ScaleToHealthFactor = 100.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Speed")
	float MinSpeed = 10.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Speed")
	float MaxSpeed = 100.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Class")
	TSubclassOf<class AAsteroid> Class;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	float SpawningRadius = 1000.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	int32 StartNumberOfAsteroids = 10;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	int32 EndNumberOfAsteroids = 200;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Spawning")
	float TimeForEndNumberOfAsteroids = 240.0f;
};
