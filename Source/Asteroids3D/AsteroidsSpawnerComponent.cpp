// Fill out your copyright notice in the Description page of Project Settings.


#include "AsteroidsSpawnerComponent.h"

#include "Asteroid.h"

void UAsteroidsSpawnerComponent::BeginPlay()
{
	Super::BeginPlay();

	//const UAsteroidsSettings* AsteroidsSettings = UAsteroidsSettings::Get();

	StartNumberOfEntities = AsteroidsSettings->StartNumberOfAsteroids;
	EndNumberOfEntities = AsteroidsSettings->EndNumberOfAsteroids;
	TimeForEndNumberOfEntities = AsteroidsSettings->TimeForEndNumberOfAsteroids;
}

void UAsteroidsSpawnerComponent::SpawnEntity()
{
	//const UAsteroidsSettings* AsteroidsSettings = UAsteroidsSettings::Get();
	
	FVector DirectionNonNormalized = FVector(FMath::FRand(), FMath::FRand(), FMath::FRand());
	DirectionNonNormalized.Normalize();

	const float MaxX = AsteroidsSettings->SpawningRadius;
	const float MaxY = AsteroidsSettings->SpawningRadius;
	const float MaxZ = AsteroidsSettings->SpawningRadius;
	const float MinX = -AsteroidsSettings->SpawningRadius;
	const float MinY = -AsteroidsSettings->SpawningRadius;
	const float MinZ = -AsteroidsSettings->SpawningRadius;

	float X = NAN;
	float Y = NAN;
	float Z = NAN;

	if (FMath::RandBool())
	{
		X = FMath::RandBool() ? MinX : MaxX;
		Y = FMath::FRandRange(MinY, MaxY);
		Z = FMath::FRandRange(MinZ, MaxZ);
	}
	else
	{
		if (FMath::RandBool())
		{
			Y = FMath::RandBool() ? MinY : MaxY;
			X = FMath::FRandRange(MinX, MaxX);
			Z = FMath::FRandRange(MinZ, MaxZ);
		}
		else
		{
			Z = FMath::RandBool() ? MinZ : MaxZ;
			X = FMath::FRandRange(MinX, MaxX);
			Y = FMath::FRandRange(MinY, MaxY);
		}
	}
	
	FVector SpawnLocation = FVector(X, Y, Z);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = GetOwner();
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(SpawnLocation);
	SpawnTransform.SetRotation(FRotator::ZeroRotator.Quaternion());
	SpawnTransform.SetScale3D(FVector::OneVector * FMath::FRandRange(AsteroidsSettings->MinScale, AsteroidsSettings->MaxScale));
	
	AAsteroid* Asteroid = GetWorld()->SpawnActor<AAsteroid>(
		AsteroidsSettings->Class.Get(), SpawnTransform, SpawnParameters);

	if (IsValid(Asteroid))
		CurrentNumberOfEntities++;
}

