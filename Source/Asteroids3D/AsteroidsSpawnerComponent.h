// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EntitySpawnerComponent.h"
#include "AsteroidsSettings.h"
#include "AsteroidsSpawnerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ASTEROIDS3D_API UAsteroidsSpawnerComponent : public UEntitySpawnerComponent
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	

private:
	virtual void SpawnEntity() override;
	

	UPROPERTY()
	const UAsteroidsSettings* AsteroidsSettings = UAsteroidsSettings::Get();
};
