// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpaceship.h"

#include "Asteroid.h"
#include "EntityWidget.h"
#include "EntityWidgetComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerSpaceship.h"


AEnemySpaceship::AEnemySpaceship()
{
	WidgetComponent = CreateDefaultSubobject<UEntityWidgetComponent>("WidgetComponent");
	WidgetComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AEnemySpaceship::BeginPlay()
{
	Super::BeginPlay();

	PlayerSpaceship = Cast<APlayerSpaceship>(UGameplayStatics::GetPlayerPawn(this, 0));
	
	CurrentForwardSpeed = MinSpeed;

	ChangeDirection();
}

void AEnemySpaceship::Fire()
{
	if (bIsFiringDisabled)
		return;
	
	Super::Fire();

	bIsFiringDisabled = true;

	FTimerHandle TimerHandle;
	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle,
		this,
		&AEnemySpaceship::EnableFiring,
		SpaceshipsSettings->EnemyFiringPeriod,
		false);
}

void AEnemySpaceship::OnActorBeginOverlapEvent(AActor* OverlappedActor, AActor* OtherActor)
{
	Super::OnActorBeginOverlapEvent(OverlappedActor, OtherActor);

	if (OtherActor)
	{
		if (AAsteroid* OtherActorAsteroid = Cast<AAsteroid>(OtherActor))
		{
			//UE_LOG(LogTemp, Warning, TEXT("AEnemySpaceship::OnActorBeginOverlapEvent"));
			GetWorld()->GetTimerManager().ClearTimer(DirectionChangeTimerHandle);
			ChangeDirection();
		}
		else if (OtherActor->ActorHasTag("MapBorder"))
			Destroy();
	}
}

void AEnemySpaceship::EnableFiring()
{
	bIsFiringDisabled = false;
}

void AEnemySpaceship::ChangeDirection()
{
	TargetDirection = FVector(FMath::FRand(), FMath::FRand(),FMath::FRand());
	if (!TargetDirection.Normalize())
		TargetDirection = FVector::ForwardVector;
	const float TimeForNextCall = FMath::FRandRange(SpaceshipsSettings->EnemyDirectionChangeMinPeriod, SpaceshipsSettings->EnemyDirectionChangeMaxPeriod);
	GetWorld()->GetTimerManager().SetTimer(DirectionChangeTimerHandle, this, &AEnemySpaceship::ChangeDirection, TimeForNextCall, false);
}

void AEnemySpaceship::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	{
		FVector TargetPoint = FVector();
		float TargetSpeed = MinSpeed;

		bool bIsFollowingPlayer = false;
		if (PlayerSpaceship)
		{
			float DistToPlayer = FVector::Dist(GetActorLocation(), PlayerSpaceship->GetActorLocation());
	
			if (DistToPlayer < SpaceshipsSettings->EnemyVisibility)
			{
				bIsFollowingPlayer = true;
			
				TargetPoint = PlayerSpaceship->GetActorLocation();
		
				if (DistToPlayer <= SpaceshipsSettings->EnemyMinDistToPlayer)
					TargetSpeed = PlayerSpaceship->GetCurrentSpeed();
				else
					TargetSpeed = MaxSpeed;

				if (DistToPlayer <= SpaceshipsSettings->EnemyFiringDistance)
					Fire();
			}
		}

		if (!bIsFollowingPlayer)
			TargetPoint = GetActorLocation() + TargetDirection * 99999.0f;

		{
			FVector DirectionToTarget = TargetPoint - GetActorLocation();
			DirectionToTarget.Normalize();
			float CurrentRoll = GetActorRotation().Roll;

			{
				FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TargetPoint);

				FRotator RotationToSet = FMath::RInterpTo(GetActorRotation(), LookAtRotation, DeltaSeconds, 1.0f);

				SetActorRotation(RotationToSet);

				CurrentForwardSpeed = UKismetMathLibrary::FInterpTo(CurrentForwardSpeed, TargetSpeed, DeltaSeconds, Acceleration);

				const FVector LocalMove = FVector(CurrentForwardSpeed * DeltaSeconds, 0.f, 0.f);

				AddActorLocalOffset(LocalMove, true);
			}

			{
				float Dot = FVector::DotProduct(GetActorForwardVector(), DirectionToTarget);
				Dot = FMath::Clamp(Dot, 0.0f, 1.0f);

				float RollToSet = FMath::Lerp(SpaceshipsSettings->EnemyMaxRoll, 1.0f, Dot);

				float Roll = FMath::FInterpTo(CurrentRoll, RollToSet, DeltaSeconds, 1.0f);
			
				float RollSign = FVector::DotProduct(GetActorRightVector(), DirectionToTarget) > 0.0f;

				FRotator Rotation = GetActorRotation();
				//Rotation.Pitch = 0.0f;
				//Rotation.Yaw = 0.0f;
				Rotation.Roll = Roll * RollSign;

				SetActorRotation(Rotation);
			}
		}
	}
}
