// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Spaceship.h"
#include "EnemySpaceship.generated.h"

/**
 * 
 */
UCLASS()
class ASTEROIDS3D_API AEnemySpaceship : public ASpaceship
{
	GENERATED_BODY()

public:
	AEnemySpaceship();
	
	
	virtual void Tick(float DeltaSeconds) override;


protected:
	virtual void BeginPlay() override;
	
	virtual void Fire() override;

	virtual void OnActorBeginOverlapEvent(AActor* OverlappedActor, AActor* OtherActor) override;
	
	
private:
	UFUNCTION()
	void EnableFiring();

	UFUNCTION()
	void ChangeDirection();


	UPROPERTY(EditAnywhere)
	class UEntityWidgetComponent* WidgetComponent = nullptr;
	
	bool bIsFiringDisabled = false;

	UPROPERTY(VisibleAnywhere)
	FVector TargetDirection = FVector();

	FTimerHandle DirectionChangeTimerHandle;
	
	UPROPERTY()
	class APlayerSpaceship* PlayerSpaceship = nullptr;
};
