// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpaceshipsSpawnerComponent.h"

#include "EnemySpaceship.h"
#include "Kismet/KismetMathLibrary.h"

void UEnemySpaceshipsSpawnerComponent::BeginPlay()
{
	Super::BeginPlay();

	StartNumberOfEntities = SpaceshipsSettings->StartNumberOfEnemies;
	EndNumberOfEntities = SpaceshipsSettings->EndNumberOfEnemies;
	TimeForEndNumberOfEntities = SpaceshipsSettings->TimeForEndNumberOfEnemies;
}

void UEnemySpaceshipsSpawnerComponent::SpawnEntity()
{
	FVector DirectionNonNormalized = FVector(FMath::FRand(), FMath::FRand(), FMath::FRand());
	DirectionNonNormalized.Normalize();

	const float MaxX = SpaceshipsSettings->EnemySpawningRadius;
	const float MaxY = SpaceshipsSettings->EnemySpawningRadius;
	const float MaxZ = SpaceshipsSettings->EnemySpawningRadius;
	const float MinX = -SpaceshipsSettings->EnemySpawningRadius;
	const float MinY = -SpaceshipsSettings->EnemySpawningRadius;
	const float MinZ = -SpaceshipsSettings->EnemySpawningRadius;

	float X = NAN;
	float Y = NAN;
	float Z = NAN;

	if (FMath::RandBool())
	{
		X = FMath::RandBool() ? MinX : MaxX;
		Y = FMath::FRandRange(MinY, MaxY);
		Z = FMath::FRandRange(MinZ, MaxZ);
	}
	else
	{
		if (FMath::RandBool())
		{
			Y = FMath::RandBool() ? MinY : MaxY;
			X = FMath::FRandRange(MinX, MaxX);
			Z = FMath::FRandRange(MinZ, MaxZ);
		}
		else
		{
			Z = FMath::RandBool() ? MinZ : MaxZ;
			X = FMath::FRandRange(MinX, MaxX);
			Y = FMath::FRandRange(MinY, MaxY);
		}
	}
	
	FVector SpawnLocation = FVector(X, Y, Z);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = GetOwner();
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
	
	FTransform SpawnTransform;
	SpawnTransform.SetLocation(SpawnLocation);
	FRotator LookToCenterRotator = UKismetMathLibrary::FindLookAtRotation(SpawnLocation, FVector::ZeroVector);
	SpawnTransform.SetRotation(LookToCenterRotator.Quaternion());
	
	AEnemySpaceship* Enemy = GetWorld()->SpawnActor<AEnemySpaceship>(
		SpaceshipsSettings->EnemySpaceshipClass.Get(), SpawnTransform, SpawnParameters);

	if (IsValid(Enemy))
	{
		CurrentNumberOfEntities++;

		Enemy->OnDestroyed.AddDynamic(this, &UEnemySpaceshipsSpawnerComponent::OnEntityDestroyedCallback);
	}
}

