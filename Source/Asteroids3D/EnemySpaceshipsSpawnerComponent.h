// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EntitySpawnerComponent.h"
#include "SpaceshipsSettings.h"
#include "EnemySpaceshipsSpawnerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ASTEROIDS3D_API UEnemySpaceshipsSpawnerComponent : public UEntitySpawnerComponent
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	
	
private:
	virtual void SpawnEntity() override;
	

	UPROPERTY()
	const USpaceshipsSettings* SpaceshipsSettings = USpaceshipsSettings::Get();
};
