// Fill out your copyright notice in the Description page of Project Settings.


#include "EntitySpawnerComponent.h"

#include "Kismet/GameplayStatics.h"

UEntitySpawnerComponent::UEntitySpawnerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UEntitySpawnerComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UEntitySpawnerComponent::SpawnEntityIfNeeded()
{
	float GameTime = UGameplayStatics::GetTimeSeconds(this);

	GameTime = FMath::Min(GameTime, TimeForEndNumberOfEntities);

	int NumberOfEntitiesToSpawn = FMath::Lerp(StartNumberOfEntities, EndNumberOfEntities, GameTime / TimeForEndNumberOfEntities);
	
	if (CurrentNumberOfEntities < NumberOfEntitiesToSpawn)
		SpawnEntity();
}

void UEntitySpawnerComponent::SpawnEntity()
{
	
}

void UEntitySpawnerComponent::OnEntityDestroyedCallback(AActor* DestroyedActor)
{
	CurrentNumberOfEntities--;
}

void UEntitySpawnerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	SpawnEntityIfNeeded();
}

