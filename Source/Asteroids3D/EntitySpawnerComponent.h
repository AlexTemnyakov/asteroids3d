// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EntitySpawnerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ASTEROIDS3D_API UEntitySpawnerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UEntitySpawnerComponent();


	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	

protected:
	virtual void BeginPlay() override;

	void SpawnEntityIfNeeded();

	virtual void SpawnEntity();
	
	UFUNCTION()
	void OnEntityDestroyedCallback(AActor* DestroyedActor);


	UPROPERTY(VisibleAnywhere)
	int32 StartNumberOfEntities = 1;

	UPROPERTY(VisibleAnywhere)
	int32 EndNumberOfEntities = 10;

	UPROPERTY(VisibleAnywhere)
	int32 CurrentNumberOfEntities = 0;

	UPROPERTY(VisibleAnywhere)
	float TimeForEndNumberOfEntities = 240.0f;
};
