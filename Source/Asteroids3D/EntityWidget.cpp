// Fill out your copyright notice in the Description page of Project Settings.


#include "EntityWidget.h"

#include "Components/TextBlock.h"

void UEntityWidget::Update(const int InHealth)
{
	HealthTextBlock->SetText(FText::FromString(FString::Printf(TEXT("%d"), InHealth)));
}
