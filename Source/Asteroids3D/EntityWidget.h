// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EntityWidget.generated.h"

/**
 * 
 */
UCLASS()
class ASTEROIDS3D_API UEntityWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void Update(const int InHealth);
	

protected:
	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UTextBlock* HealthTextBlock = nullptr;
};
