// Fill out your copyright notice in the Description page of Project Settings.


#include "EntityWidgetComponent.h"

#include "Asteroids3DEntityInterface.h"
#include "EntityWidget.h"
#include "GameDeveloperSettings.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

void UEntityWidgetComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                           FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	{
		if (APlayerCameraManager* PlayerCameraManager = UGameplayStatics::GetPlayerCameraManager(this, 0))
			SetWorldRotation(UKismetMathLibrary::FindLookAtRotation(GetComponentLocation(), PlayerCameraManager->GetCameraLocation()));

		if (IAsteroids3DEntityInterface* Asteroids3DEntity = Cast<IAsteroids3DEntityInterface>(GetOwner()))
			if (UEntityWidget* EntityWidget = Cast<UEntityWidget>(GetUserWidgetObject()))
				EntityWidget->Update(Asteroids3DEntity->GetHealth());
	}
}

void UEntityWidgetComponent::BeginPlay()
{
	Super::BeginPlay();

	if (const UGameDeveloperSettings* GameDeveloperSettings = UGameDeveloperSettings::Get())
		SetWidgetClass(GameDeveloperSettings->EntityWidgetClass.Get());
}
