// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Asteroids3DHUD.h"
#include "EntityWidget.h"
#include "Engine/DeveloperSettings.h"
#include "GameDeveloperSettings.generated.h"

/**
 * 
 */
UCLASS(Config=Game, defaultconfig)
class ASTEROIDS3D_API UGameDeveloperSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	static const UGameDeveloperSettings* Get() { return GetDefault<UGameDeveloperSettings>(); }

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Entity Widget")
	TSubclassOf<UEntityWidget> EntityWidgetClass;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "HUD Widget")
	TSubclassOf<class UAsteroids3DHUD> HUDClass;
};
