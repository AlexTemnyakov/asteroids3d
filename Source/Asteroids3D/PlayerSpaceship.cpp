// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerSpaceship.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

APlayerSpaceship::APlayerSpaceship()
{
	PrimaryActorTick.bCanEverTick = true;

	// Create a spring arm component
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm0"));
	SpringArm->SetupAttachment(RootComponent);	// Attach SpringArm to RootComponent
	SpringArm->TargetArmLength = 160.0f; // The camera follows at this distance behind the character	
	SpringArm->SocketOffset = FVector(0.f,0.f,60.f);
	SpringArm->bEnableCameraLag = false;	// Do not allow camera to lag
	SpringArm->CameraLagSpeed = 15.f;

	// Create camera component 
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);	// Attach the camera
	Camera->bUsePawnControlRotation = false; // Don't rotate camera with controller
}

void APlayerSpaceship::BeginPlay()
{
	Super::BeginPlay();

	Health = SpaceshipsSettings->PlayerSpaceshipHealth;
}

void APlayerSpaceship::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ProcessInputValues();

	{
		const FVector LocalMove = FVector(CurrentForwardSpeed * DeltaTime, 0.f, 0.f);

		AddActorLocalOffset(LocalMove, true);

		FRotator DeltaRotation(0,0,0);
		DeltaRotation.Pitch = CurrentPitchSpeed * DeltaTime;
		DeltaRotation.Yaw = CurrentYawSpeed * DeltaTime;
		DeltaRotation.Roll = CurrentRollSpeed * DeltaTime;

		AddActorLocalRotation(DeltaRotation);
	}

	if (Health <= 0)
		Destroy();
}

void APlayerSpaceship::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	PlayerInputComponent->BindAxis("Thrust", this, &APlayerSpaceship::ThrustInput);
	PlayerInputComponent->BindAxis("Pitch", this, &APlayerSpaceship::PitchInput);
	PlayerInputComponent->BindAxis("Roll", this, &APlayerSpaceship::RollInput);
	PlayerInputComponent->BindAxis("Yaw", this, &APlayerSpaceship::YawInput);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerSpaceship::Fire);
}

void APlayerSpaceship::ProcessInputValues()
{
	{
		// Is there any input?
		bool bHasInput = !FMath::IsNearlyEqual(ThrustInputValue, 0.f);
		// If input is not held down, reduce speed
		float CurrentAcc = bHasInput ? (ThrustInputValue * Acceleration) : (-0.5f * Acceleration);
		// Calculate new speed
		float NewForwardSpeed = CurrentForwardSpeed + (GetWorld()->GetDeltaSeconds() * CurrentAcc);
		// Clamp between MinSpeed and MaxSpeed
		CurrentForwardSpeed = FMath::Clamp(NewForwardSpeed, MinSpeed, MaxSpeed);
	}

	{
		// Target pitch speed is based in input
		float TargetPitchSpeed = (PitchInputValue * TurnSpeed * -1.f);

		// When steering, we decrease pitch slightly
		TargetPitchSpeed += (FMath::Abs(CurrentYawSpeed) * -0.2f);

		// Smoothly interpolate to target pitch speed
		CurrentPitchSpeed = FMath::FInterpTo(CurrentPitchSpeed, TargetPitchSpeed, GetWorld()->GetDeltaSeconds(), 2.f);
	}

	{
		float Value = 0.0f;
		if (FMath::Abs(RollInputValue) > FMath::Abs(YawInputValue))
			Value = RollInputValue;
		else
			Value = YawInputValue;
		// Target yaw speed is based on input
		float TargetYawSpeed = (Value * TurnSpeed);

		// Smoothly interpolate to target yaw speed
		CurrentYawSpeed = FMath::FInterpTo(CurrentYawSpeed, TargetYawSpeed, GetWorld()->GetDeltaSeconds(), 2.f);
	}

	{
		// Is there any left/right input?
		const bool bIsTurning = FMath::Abs(RollInputValue) > 0.2f;

		// If turning, yaw value is used to influence roll
		// If not turning, roll to reverse current roll value.
		float TargetRollSpeed = bIsTurning ? (CurrentYawSpeed * 0.5f) : (GetActorRotation().Roll * -2.f);

		// Smoothly interpolate roll speed
		CurrentRollSpeed = FMath::FInterpTo(CurrentRollSpeed, TargetRollSpeed, GetWorld()->GetDeltaSeconds(), 2.f);
	}
}

void APlayerSpaceship::ThrustInput(float Value)
{
	ThrustInputValue = Value;
}

void APlayerSpaceship::PitchInput(float Value)
{
	PitchInputValue = Value;
}

void APlayerSpaceship::RollInput(float Value)
{
	RollInputValue = Value;
}

void APlayerSpaceship::YawInput(float Value)
{
	YawInputValue = Value;
}

