// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Spaceship.h"
#include "PlayerSpaceship.generated.h"
UCLASS()
class ASTEROIDS3D_API APlayerSpaceship : public ASpaceship
{
	GENERATED_BODY()

public:
	APlayerSpaceship();

	
	virtual void Tick(float DeltaTime) override;

	
protected:
	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


private:
	void ProcessInputValues();
	
	UFUNCTION()
	void ThrustInput(float Value);

	UFUNCTION()
	void PitchInput(float Value);

	UFUNCTION()
	void RollInput(float Value);

	UFUNCTION()
	void YawInput(float Value);
	

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArm = nullptr;

	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera = nullptr;

	UPROPERTY(VisibleAnywhere)
	float YawInputValue = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float RollInputValue = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float PitchInputValue = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float ThrustInputValue = 0.0f;
};
