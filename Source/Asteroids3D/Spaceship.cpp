// Fill out your copyright notice in the Description page of Project Settings.


#include "Spaceship.h"

#include "SpaceshipProjectile.h"

// Sets default values
ASpaceship::ASpaceship()
{
	PrimaryActorTick.bCanEverTick = true;

	PlaneMesh = CreateDefaultSubobject<UStaticMeshComponent>("PlaneMesh0");
	RootComponent = PlaneMesh;

	MuzzlePoint = CreateDefaultSubobject<USceneComponent>("Muzzle");
	MuzzlePoint->AttachToComponent(PlaneMesh, FAttachmentTransformRules::KeepRelativeTransform);
}

void ASpaceship::BeginPlay()
{
	Super::BeginPlay();

	OnActorHit.AddDynamic(this, &ASpaceship::OnActorHitEvent);
	OnActorBeginOverlap.AddDynamic(this, &ASpaceship::OnActorBeginOverlapEvent);

	Acceleration = SpaceshipsSettings->Acceleration;
	TurnSpeed = SpaceshipsSettings->TurnSpeed;
	MaxSpeed = SpaceshipsSettings->MaxSpeed;
	MinSpeed = SpaceshipsSettings->MinSpeed;
	CurrentForwardSpeed = MaxSpeed;

	CollisionDamage = SpaceshipsSettings->CollisionDamage;
	Health = SpaceshipsSettings->Health;
}

void ASpaceship::Fire()
{
	//UE_LOG(LogTemp, Warning, TEXT("fire"));
	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = GetInstigator();

	ASpaceshipProjectile* Projectile = GetWorld()->SpawnActor<ASpaceshipProjectile>(
		SpaceshipProjectilesSettings->SpaceshipProjectileClass,
		MuzzlePoint->GetComponentLocation(),
		MuzzlePoint->GetComponentRotation(),
		SpawnParams);
	
	if (Projectile)
	{
		const FVector LaunchDirection = MuzzlePoint->GetComponentRotation().Vector();
		Projectile->FireInDirection(LaunchDirection, SpaceshipProjectilesSettings->DefaultDamage);
	}
}

void ASpaceship::OnActorHitEvent(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	// if (const ASpaceshipProjectile* OtherActorAsProjectile = Cast<ASpaceshipProjectile>(OtherActor))
	// 	if (OtherActorAsProjectile->GetOwner() == this)
	// 		return;
	//
	// FRotator CurrentRotation = GetActorRotation();
	// SetActorRotation(FQuat::Slerp(CurrentRotation.Quaternion(), Hit.Normal.ToOrientationQuat(), 0.025f));
}

void ASpaceship::OnActorBeginOverlapEvent(AActor* OverlappedActor, AActor* OtherActor)
{
	
}

void ASpaceship::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Health <= 0)
		Destroy();
}

void ASpaceship::ApplyDamage(const float AppliedDamage)
{
	Health -= AppliedDamage;
}