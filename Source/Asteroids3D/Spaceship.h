// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Asteroids3DEntityInterface.h"
#include "SpaceshipProjectilesSettings.h"
#include "SpaceshipsSettings.h"
#include "Spaceship.generated.h"

UCLASS()
class ASTEROIDS3D_API ASpaceship : public APawn, public IAsteroids3DEntityInterface
{
	GENERATED_BODY()
	
public:	
	ASpaceship();


	virtual void Tick(float DeltaTime) override;

	virtual void ApplyDamage(const float AppliedDamage) override;

	float GetCurrentSpeed() { return CurrentForwardSpeed; }
	
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void Fire();

	UFUNCTION()
	virtual void OnActorHitEvent(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	virtual void OnActorBeginOverlapEvent(AActor* OverlappedActor, AActor* OtherActor);


	UPROPERTY(EditAnywhere)
	class USceneComponent* MuzzlePoint = nullptr;	
	
	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* PlaneMesh = nullptr;

	UPROPERTY(EditAnywhere)
	float Acceleration = 0.0f;

	UPROPERTY(EditAnywhere)
	float TurnSpeed = 0.0f;

	UPROPERTY(EditAnywhere)
	float MaxSpeed = 0.0f;

	UPROPERTY(EditAnywhere)
	float MinSpeed = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float CurrentForwardSpeed = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float CurrentYawSpeed = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float CurrentPitchSpeed = 0.0f;

	UPROPERTY(VisibleAnywhere)
	float CurrentRollSpeed = 0.0f;

	UPROPERTY()
	const USpaceshipProjectilesSettings* SpaceshipProjectilesSettings = USpaceshipProjectilesSettings::Get();
	
	UPROPERTY()
    const USpaceshipsSettings* SpaceshipsSettings = USpaceshipsSettings::Get();
};
