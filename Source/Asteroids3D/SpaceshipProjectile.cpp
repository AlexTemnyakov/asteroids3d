// Fill out your copyright notice in the Description page of Project Settings.


#include "SpaceshipProjectile.h"

#include "Asteroids3DEntityInterface.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

ASpaceshipProjectile::ASpaceshipProjectile()
{
	PrimaryActorTick.bCanEverTick = true;

	{
		ProjectileCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollisionComponent"));
		ProjectileCollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("Projectile"));
		ProjectileCollisionComponent->OnComponentHit.AddDynamic(this, &ASpaceshipProjectile::OnHit);
		RootComponent = ProjectileCollisionComponent;
	}

	{
		ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
		ProjectileMovementComponent->SetUpdatedComponent(ProjectileCollisionComponent);
		ProjectileMovementComponent->InitialSpeed = SpaceshipProjectilesSettings->InitialSpeed;
		ProjectileMovementComponent->MaxSpeed = SpaceshipProjectilesSettings->MaxSpeed;
		ProjectileMovementComponent->bRotationFollowsVelocity = true;
		ProjectileMovementComponent->bShouldBounce = true;
		ProjectileMovementComponent->Bounciness = 0.3f;
		ProjectileMovementComponent->ProjectileGravityScale = 0.0f;
	}

	{
		ProjectileMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMeshComponent"));
		ProjectileMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	}

	InitialLifeSpan = SpaceshipProjectilesSettings->LifeSpan;
}

void ASpaceshipProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASpaceshipProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpaceshipProjectile::FireInDirection(const FVector& InShootDirection, const float InDamage)
{
	Damage = InDamage;

	ProjectileMovementComponent->Velocity = InShootDirection * ProjectileMovementComponent->InitialSpeed;
}

void ASpaceshipProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor == GetOwner())
		return;
	
	//UE_LOG(LogTemp, Warning, TEXT("ASpaceshipProjectile::OnHit, %s"), *OtherActor->GetFName().ToString());
	if (IAsteroids3DEntityInterface* Asteroids3DEntity = Cast<IAsteroids3DEntityInterface>(OtherActor))
	{
		Asteroids3DEntity->ApplyDamage(Damage);
	}
	
	Destroy();
}

