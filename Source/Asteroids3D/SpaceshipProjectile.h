// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpaceshipProjectilesSettings.h"
#include "GameFramework/Actor.h"
#include "SpaceshipProjectile.generated.h"

UCLASS()
class ASTEROIDS3D_API ASpaceshipProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	ASpaceshipProjectile();


	virtual void Tick(float DeltaTime) override;

	void FireInDirection(const FVector& InShootDirection, const float InDamage);
	

protected:
	virtual void BeginPlay() override;


private:
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

	
	UPROPERTY(EditAnywhere)
	class USphereComponent* ProjectileCollisionComponent = nullptr;

	UPROPERTY(VisibleAnywhere)
	class UProjectileMovementComponent* ProjectileMovementComponent = nullptr;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* ProjectileMeshComponent = nullptr;

	UPROPERTY(VisibleAnywhere)
	float Damage = 0.0f;

	// Projectile material
	//UPROPERTY(VisibleDefaultsOnly)
	//UMaterialInstanceDynamic* ProjectileMaterialInstance;

	UPROPERTY()
	const USpaceshipProjectilesSettings* SpaceshipProjectilesSettings = USpaceshipProjectilesSettings::Get();
};
