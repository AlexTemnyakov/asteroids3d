// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "SpaceshipProjectilesSettings.generated.h"

/**
 * 
 */
UCLASS(Config=Game, defaultconfig)
class ASTEROIDS3D_API USpaceshipProjectilesSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	static const USpaceshipProjectilesSettings* Get() { return GetDefault<USpaceshipProjectilesSettings>(); }

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Meta")
	TSubclassOf<class ASpaceshipProjectile> SpaceshipProjectileClass;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "LifeSpan")
	float LifeSpan = 3.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Speed")
	float InitialSpeed = 10000.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Speed")
	float MaxSpeed = 10000.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Speed")
	float DefaultDamage = 50.0f;
};
