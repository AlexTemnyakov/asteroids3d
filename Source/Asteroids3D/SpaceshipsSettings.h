// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "SpaceshipsSettings.generated.h"

/**
 * 
 */
UCLASS(Config=Game, defaultconfig)
class ASTEROIDS3D_API USpaceshipsSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:
	static const USpaceshipsSettings* Get() { return GetDefault<USpaceshipsSettings>(); }

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Health")
	float Health = 10.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Player|Health")
	float PlayerSpaceshipHealth = 10.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Common|Damage")
	float CollisionDamage = 2.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Common|Speed")
	float Acceleration = 500.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Common|Speed")
	float TurnSpeed = 50.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Common|Speed")
	float MaxSpeed = 4000.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Common|Speed")
	float MinSpeed = 500.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Meta")
	TSubclassOf<class AEnemySpaceship> EnemySpaceshipClass;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Meta")
	TSubclassOf<class UUserWidget> EnemySpaceshipWidgetClass;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Transform")
	float EnemyMaxRoll = 45.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Interaction with player")
	float EnemyMinDistToPlayer = 100.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Interaction with player")
	float EnemyVisibility = 2000.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Firing")
	float EnemyFiringDistance = 300.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Firing")
	float EnemyFiringPeriod = 1.f;	

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Flight direction")
	float EnemyDirectionChangeMinPeriod = 10.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Flight direction")
	float EnemyDirectionChangeMaxPeriod = 20.f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Spawning")
	int32 StartNumberOfEnemies = 1;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Spawning")
	int32 EndNumberOfEnemies = 20;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Spawning")
	float TimeForEndNumberOfEnemies = 240.0f;

	UPROPERTY(Config, EditAnywhere, BlueprintReadOnly, Category = "Enemy|Spawning")
	float EnemySpawningRadius = 1000.0f;
};
